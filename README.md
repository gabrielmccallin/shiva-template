# `shiva-init`
A template to get started with [`shiva`](https://shiva.gabrielmccallin.now.sh)

## Usage
- `npm i`  
to install dependencies  

- `npm start`  
to launch a dev server at `localhost:1234` and reload changes  

- `npm run build`  
to create a production build in `/dist`  

- `npm t`  
to run `*.test.ts`

## Container
Create html nodes with JavaScript object syntax.

```javascript
const description = container({
    children: 'Full resolution',
    tagName: 'span',
    id: 'image-description'
});
```

Initialise the application with a root container and attach nodes with the `children` property. 

```javascript
const app = () => {
    container({
        root: true, // attach this node and all children to the DOM
        children: description
    });
};

```

## Styles
[`emotion`](https://emotion.sh) with object styles. 

```javascript
import { css } from 'emotion';

container({
    className: css({
        fontSize: '2rem',
        fontWeight: 'bold'
    })
});
```

Can use `emotion` tagged template literal style as well.

## Style helper
`tag()` to pass a style object as the first parameter. Avoids having to specify a `className` property.

```javascript
import { tag } from './utils/tag';

const container = tag({
    fontSize: '2rem'
}, {
    children: 'Full resolution',
    tagName: 'span',
    id: 'image-description'
});

```

## Local state
`useState()` from `shiva` to bind views to data sources; data can be a primitive value or nested.

```javascript
import { useState } from 'shiva';

const [state, setState] = useState({
        title: '',
        desc: '',
        url: ''
    });

// this is called by something asynchronous 
const handler = (data) => {
    const { desc, title, url } = data;

    // this will set the children of container to the new values
    setState({ desc, title, url });
};

const container = container({
    children: [
        tag({
            id: 'title'
        }, {
            textContent: state.desc
        }), 
        tag({
            id: 'desc'
        }, {
            textContent: state.title
        }),
        tag({
            id: 'url'
        }, {
            textContent: state.url
        })
    ]
});

```
See nested data structure example [here](/nested).

## Global state with streams
[`flyd`](https://github.com/paldepind/flyd) for application state.  

Create stores for different parts of the application and observe / update through that store. Updates to the store are merged with `Object.assign` in this example but any transform can take place here.

### Create a store
```javascript
import { scan, stream } from 'flyd';

const observable = stream();

export const store = {
    observe(callback) {
        scan((accumulator, current) => 
            Object.assign(accumulator, current), {}, observable)
            .map(callback);
    },
    update(payload) {
        observable(payload);
    }
};

```

### Update the store
```javascript
import { update } from './store';

update(value);
```

### Observe the store
```javascript
import { observe } from './store';

observe(value => doSomething(value));
```

See example [here](/stream).


## History
[`history`](https://npmjs.com/package/history) for routing.
```javascript
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

// respond to URL entrypoint or change
history.listen(location => { doSomething(location) })

// update URL
history.push(`/${payload}`, { some: 'state' });

```

## Visualise bundle size
[`parcel-plugin-bundle-visualiser`](https://www.npmjs.com/package/parcel-plugin-bundle-visualiser) to create a report showing the size of dependencies.

Runs automatically on `npm run build` and creates `report.html` in the build folder.