import { Interpolation } from 'emotion';

export const globalStyle: Interpolation = {
    '*': {
        boxSizing: 'border-box'
    },
    body: {
        margin: 0,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
        padding: '2rem',
        backgroundColor: '#e9eef3',
        color: '#3d3550'
    }
};
