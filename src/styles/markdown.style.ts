import { Interpolation } from 'emotion';

export const markdownStyle: Interpolation = {
    a: {
        code: {
            color: '#6563e0'
        },
        color: '#6563e0'
    },
    h1: {
        margin: '0',
        fontSize: '3rem',
        fontWeight: 500
    },
    h2: {
        fontWeight: 500,
        margin: '4rem 0 0 0'
    },
    h3: {
        fontWeight: 500,
        margin: '4rem 0 0 0'
    },
    p: {
        margin: '1rem 0 0 0',
        lineHeight: '1.7'
    },
    pre: {
        padding: '1rem',
        backgroundColor: '#f7f7ff',
        borderRadius: '0.5rem',
        whiteSpace: 'pre-wrap',
        margin: '1rem 0 0 0',
        border: '1px solid #fff',
        code: {
            border: '0px solid'
        }
    },
    code: {
        paddingLeft: '0.25rem',
        paddingRight: '0.25rem',
        border: '1px solid #fff',
        backgroundColor: '#f7f7ff',
        borderRadius: '0.2rem',
        whiteSpace: 'pre-wrap',
        fontFamily: 'Overpass Mono, monospace',
        lineHeight: '1.7',
        color: 'darkcyan'
    }
};
