import { css, Interpolation } from 'emotion';
import { container } from 'shiva';
import { ContainerSchema } from 'shiva/container';

export const tag = <T extends HTMLElement>(style: Interpolation, { ...options }: ContainerSchema<T>) => {
    const templateStrings = style as Interpolation;
    return container({
        ...options,
        className: css(templateStrings)
    });
};
