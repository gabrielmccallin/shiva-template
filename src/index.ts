import { injectGlobal } from 'emotion';
import { routes } from './examples/routes/routes';
import { globalStyle } from './styles/global.style';
import { tag } from './utils/tag';

injectGlobal(globalStyle);

const app = () => {
    tag({
        display: 'flex'
    }, {
        root: true,
        // routes example
        children: routes()
    });
};

app();
