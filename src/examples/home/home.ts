// import { markdown } from '../../../utils/markdown';
// import marked from 'marked';
// import { useState } from 'shiva';
import { markdownStyle } from '../../styles/markdown.style';
import { tag } from '../../utils/tag';
import { router } from '../routes/router';
import hljs from 'highlight.js/lib/highlight';
import javascript from 'highlight.js/lib/languages/javascript';
import 'highlightjs/styles/atom-one-light.css';
import readme from '../../../README.md';
// import showdown from 'showdown';

// const textContent = `
// ## [shiva](https://npmjs.com/package/shiva) is a minimal JavaScript library for building user interfaces.

// This template includes examples of how to achieve some common tasks when building web sites or applications:
// - routing
// - global state with streams
// - css-in-js styling
// - local state with nested data structures
// - \`tag()\` utility to abstract styling

// #### Examples
// - [Global store using a stream](<> "stream")
// - [Local state with a nested data structure](<> "nest")
// `;

export const home = () => {
    hljs.registerLanguage('javascript', javascript);

    const preventDefault = (e: Event, callback) => {
        const target = e.target as HTMLAnchorElement;
        if (target && target.nodeName === 'A' && target.title) {
            e.preventDefault();
            callback(target.title);
        }
    };

    const markDownNode = tag(markdownStyle, {
        innerHTML: readme,
        events: {
            type: 'click',
            handler: e => preventDefault(e, router.update)
        }
    });

    markDownNode.querySelectorAll('pre code').forEach((block) => {
        hljs.highlightBlock(block);
    });

    return markDownNode;
};
