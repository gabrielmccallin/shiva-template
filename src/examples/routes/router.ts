import { createBrowserHistory } from 'history';
import { stream } from 'flyd';

const observable = stream();

const history = createBrowserHistory();
history.listen((location) => {
    observable(location.pathname);
});

export const router = {
    observe(callback) {
        observable.map(callback);
    },
    update(payload) {
        // observable(payload);

        history.push(`/${payload}`, { some: 'state' });
    },
    location() {
        return history.location.pathname;
    }
};
