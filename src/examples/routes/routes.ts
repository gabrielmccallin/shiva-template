import { useState } from 'shiva';
import { tag } from '../../utils/tag';
import { home } from '../home/home';
import { nested } from '../nested/nested';
import { stream } from '../stream/stream';
import { router } from './router';

const paths = {
    '/': home,
    '/nested': nested,
    '/stream': stream
};

export const routes = () => {
    const [page, setPage] = useState(router.location(), page => {
        return (paths[page] || paths['/'])();
    });

    router.observe(setPage);

    return tag({
        maxWidth: '800px',
        margin: 'auto'
    }, {
        children: page
    });
};
