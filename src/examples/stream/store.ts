import { scan, stream } from 'flyd';

const observable = stream();

export const store = {
    observe(callback) {
        scan((accumulator, current) => Object.assign(accumulator, current), {}, observable)
            .map(callback);
    },
    update(payload) {
        observable(payload);
    }
};
