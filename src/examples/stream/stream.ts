import { provider } from './provider';
import { consumer } from './consumer';
import { tag } from '../../utils/tag';

export const stream = () => {
    return tag({
        display: 'flex',
        flexWrap: 'wrap'
    }, {
        children: [
            provider(),
            consumer()
        ]
    });
};
