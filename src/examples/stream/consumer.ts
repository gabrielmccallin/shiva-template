import { tag } from '../../utils/tag';
import { styles } from './styles';
import { useState } from 'shiva';
import { store } from './store';

const title = tag({
    marginBottom: '2rem',
    ...styles.title as {}
}, {
    children: 'Consumer'
});

const [text, setText] = useState('hello', (state) => {
    return JSON.stringify(state, null, 2);
});

const textarea = tag({
    ...styles.textarea as {},
    wordBreak: 'break-word',
    whiteSpace: 'pre'
}, {
    tagName: 'div',
    children: text
});

store.observe(setText);

export const consumer = () => {
    return tag({
        ...styles.column as {}
    }, {
        children: [
            title,
            textarea
        ]
    });
};
