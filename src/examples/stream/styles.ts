import { Interpolation } from 'emotion';

const theme = {
    border: 'solid 1px #d08d8d',
    colors: {
        backgroundLight: '#c17c7c',
        highlight: 'blue'
    },
    font: {
        size: {
            body: '1rem',
            title: '2rem'
        },
        color: {
            primary: 'white'
        }
    }
};

interface styleSchema {
    title: Interpolation;
    textarea: Interpolation;
    button: Interpolation;
    column: Interpolation;
}

export const styles: styleSchema = {
    title: {
        fontSize: theme.font.size.title,
        color: theme.font.color.primary,
        fontWeight: 'bold'
    },
    textarea: {
        backgroundColor: theme.colors.backgroundLight,
        border: theme.border,
        height: '300px',
        width: '100%',
        fontSize: theme.font.size.body,
        color: theme.font.color.primary,
        resize: 'none',
        padding: '1rem',
        marginBottom: '2rem'
    },
    button: {
        fontSize: theme.font.size.body,
        color: theme.font.color.primary,
        fontWeight: 'bold',
        backgroundColor: theme.colors.highlight,
        padding: '1rem',
        cursor: 'pointer',
        border: theme.border,
        borderRadius: '0.3rem'
    },
    column: {
        '@media (max-width: 600px)': {
            flex: '0 0 100%'
        },
        flex: '0 0 50%'
    }
};
