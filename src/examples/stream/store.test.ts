import { store } from './store';
import { container } from 'shiva';

describe('store', () => {
    it('publish and subscribe to the store', () => {
        const fixture = {
            hello: 'there'
        };

        const testContainer = container({
            textContent: 'hello'
        });

        let data = {};

        store.observe((storeData) => {
            data = storeData;
        });

        store.update(fixture);

        expect(data).toEqual(fixture);
        expect(testContainer.textContent).toEqual('hello');
    });
});
