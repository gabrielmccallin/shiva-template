import { tag } from '../../utils/tag';
import { styles } from './styles';
import { store } from './store';

const textarea = tag<HTMLTextAreaElement>({
    ...styles.textarea as {}
}, {
    tagName: 'textarea',
    value: `{
  "hello": "fd",
  "there": "wow"
}`
});

const title = tag({
    marginBottom: '2rem',
    ...styles.title as {}
}, {
    children: 'Provider'
});

const button = tag({
    ...styles.button as {}
}, {
    tagName: 'button',
    children: 'Update state',
    events: {
        type: 'click',
        handler: () => store.update((JSON.parse((textarea as HTMLTextAreaElement).value)))
    }
});

const buttonCenter = tag({
    textAlign: 'right'
}, {
    tagName: 'div',
    children: button
});

export const provider = () => {
    return tag({
        ...styles.column as {},
        paddingRight: '2rem'
    }, {
        children: [
            title,
            textarea,
            buttonCenter
        ]
    });
};
