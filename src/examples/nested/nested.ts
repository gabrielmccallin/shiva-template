import { container, useState } from 'shiva';
import { tag } from '../../utils/tag';
import { data } from './data';

export const nested = () => {
    const [state, setState] = useState({
        title: '',
        desc: '',
        url: ''
    });

    const { url, title, desc } = data;
    setState({ title, url, desc });

    return [
        tag({
            fontSize: '1.5rem'
        }, {
            children: state.title
        }),
        tag<HTMLImageElement>({
            maxWidth: '100%',
            marginTop: '1rem',
            maxHeight: '95vh',
            boxShadow: '0 0 14px rgba(44, 34, 125, 0.2)'
        }, {
            tagName: 'img',
            src: state.url,
            alt: state.title
        }),
        tag({
            marginTop: '1rem',
            marginBottom: '3rem',
            columns: '300px',
            lineHeight: '1.7',
            textAlign: 'justify',
            columnGap: '2rem'
        }, {
            children: state.desc
        }),
        container({
            children: 'Full resolution: ',
            tagName: 'span'
        }),
        tag<HTMLAnchorElement>({
            color: 'darkviolet',
            marginTop: '2rem'
        }, {
            href: state.url,
            children: state.url,
            tagName: 'a'
        })
    ];
};
